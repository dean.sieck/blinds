/*
  make sure all electrical components work
*/

// pins for the encoder inputs
#define RH_ENCODER_A 3
#define RH_ENCODER_B 5
#define LH_ENCODER_A 2
#define LH_ENCODER_B 4
//pins for motors
static int rightMotor_up = 8;
static int rightMotor_down = 9;
static int leftMotor_up = 7;
static int leftMotor_down = 6;
//pins for photo resistor, buttons & LED
static int light_Pin = 7;
static int led_Pin = 10;
static int buttonUp_Pin = 11;
static int buttonDown_Pin = 12;


// variables for motor encoder
volatile  long leftCount = 0;
volatile  long rightCount = 0;
unsigned long RH_prevTime;
unsigned long LH_prevTime;
unsigned long RH_time = 0;
unsigned long LH_time = 0;
int rightTop = 1000;
int leftTop = 1000;
int stall = 10;

//variables for light sensing
int lowlight = 300;
int samplerate = 10000;
int light;
int sampletime = 10000;

//variables for current state
bool closed = 0;
int buttonOverride = 0;

void setup() {
  pinMode(LH_ENCODER_A, INPUT);
  pinMode(LH_ENCODER_B, INPUT);
  pinMode(RH_ENCODER_A, INPUT);
  pinMode(RH_ENCODER_B, INPUT);
  pinMode(rightMotor_up, OUTPUT);
  pinMode(rightMotor_down, OUTPUT);
  pinMode(leftMotor_up, OUTPUT);
  pinMode(leftMotor_down, OUTPUT);
  pinMode(buttonUp_Pin, INPUT);
  pinMode(buttonDown_Pin, INPUT);
  pinMode(led_Pin, OUTPUT);

  // initialize hardware interrupts
  attachInterrupt(digitalPinToInterrupt(LH_ENCODER_A), leftEncoderEvent, CHANGE);
  attachInterrupt(digitalPinToInterrupt(RH_ENCODER_A), rightEncoderEvent, CHANGE);

  digitalWrite(rightMotor_up, LOW);
  digitalWrite(rightMotor_down, LOW);
  digitalWrite(leftMotor_up, LOW);
  digitalWrite(leftMotor_down, LOW);

  Serial.begin(250000);

}

void loop() {
  light = 0;
  int i = 0;
  if (digitalRead (buttonUp_Pin) == HIGH || digitalRead (buttonDown_Pin) == HIGH) {
    ButtonPush();
    i++;
  }
  light = analogRead(light_Pin);
  Serial.print("light: ");
  Serial.println(light);


  openBlinds();
  delay (2000);
  closeBlinds();
  delay (2000);


}

void openBlinds()
{
  buttonOverride = 0;
  closed = 0;
  digitalWrite(rightMotor_up, HIGH);
  digitalWrite(leftMotor_up, HIGH);
  delay (100);
  while (rightCount < rightTop || leftCount < leftTop) {
    if (RH_time > stall || LH_time > stall) {
      CheckStalled();
    }
    if (rightCount > rightTop) {
      digitalWrite(rightMotor_up, LOW);
    }
    if (leftCount > leftTop) {
      digitalWrite(leftMotor_up, LOW);
    }
  }
  digitalWrite(rightMotor_up, LOW);
  digitalWrite(leftMotor_up, LOW);
}


void closeBlinds()
{
  buttonOverride = 0;
  closed = 1;
  digitalWrite(rightMotor_down, HIGH);
  digitalWrite(leftMotor_down, HIGH);
  delay(100);
  while (rightCount > 0 || leftCount > 0) {
    if (RH_time > stall || LH_time > stall) {
      CheckStalled();
    }
    if (rightCount < 0) {
      digitalWrite(rightMotor_down, LOW);
    }
    if (leftCount  < 0) {
      digitalWrite(leftMotor_down, LOW);
    }
  }
  digitalWrite(rightMotor_down, LOW);
  digitalWrite(leftMotor_down, LOW);
}

void ButtonPush() {
  if (digitalRead (buttonUp_Pin) == HIGH) {
    Serial.println("up button pushed");
    delay (500);
  }
  if (digitalRead (buttonDown_Pin) == HIGH) {
    Serial.println("down button pushed");
    delay (500);
  }


}

void CheckStalled() {
  digitalWrite(led_Pin, HIGH);
  //stop all  motors
  digitalWrite(rightMotor_up, LOW);
  digitalWrite(rightMotor_down, LOW);
  digitalWrite(leftMotor_up, LOW);
  digitalWrite(leftMotor_down, LOW);

  //move blinds down
  digitalWrite(rightMotor_down, HIGH);
  digitalWrite(leftMotor_down, HIGH);
  delay (500);
  digitalWrite(rightMotor_down, LOW);
  digitalWrite(leftMotor_down, LOW);

  //move blinds back up
  digitalWrite(rightMotor_up, HIGH);
  digitalWrite(leftMotor_up, HIGH);
  delay (500);
  digitalWrite(rightMotor_up, LOW);
  digitalWrite(leftMotor_up, LOW);

  //check if right still stalled
  if (RH_time > stall && (rightTop - rightCount) < 500) {
    rightTop = rightCount;
  } else if (RH_time > stall) {
    //turn off all motors
    digitalWrite(rightMotor_up, LOW);
    digitalWrite(rightMotor_down, LOW);
    digitalWrite(leftMotor_up, LOW);
    digitalWrite(leftMotor_down, LOW);
    while (1) {
      //flash LED
    }
  }
  //check if left still stalled
  if (LH_time > stall && (leftTop - leftCount) < 500) {
    leftTop = leftCount;
  } else if (LH_time > stall) {
    //turn off all motors
    digitalWrite(rightMotor_up, LOW);
    digitalWrite(rightMotor_down, LOW);
    digitalWrite(leftMotor_up, LOW);
    digitalWrite(leftMotor_down, LOW);
    while (1) {
      //flash LED
    }
  }

  digitalWrite(led_Pin, LOW);
}

// encoder event for the interrupt call
void leftEncoderEvent() {
  if (digitalRead(LH_ENCODER_A) == HIGH) {
    if (digitalRead(LH_ENCODER_B) == LOW) {
      leftCount++;
    } else {
      leftCount--;
    }
  } else {
    if (digitalRead(LH_ENCODER_B) == LOW) {
      leftCount--;
    } else {
      leftCount++;
    }
  }
  LH_time = millis() - LH_prevTime;
  LH_prevTime = millis();
}

// encoder event for the interrupt call
void rightEncoderEvent() {
  if (digitalRead(RH_ENCODER_A) == HIGH) {
    if (digitalRead(RH_ENCODER_B) == LOW) {
      rightCount++;
    } else {
      rightCount--;
    }
  } else {
    if (digitalRead(RH_ENCODER_B) == LOW) {
      rightCount--;
    } else {
      rightCount++;
    }
  }
  RH_time = millis() - RH_prevTime;
  RH_prevTime = millis();
}
