/*
  blind control with encoder - with non-interrupt button
*/

// pins for the encoder inputs
#define RH_ENCODER_A 3
#define RH_ENCODER_B 5
#define LH_ENCODER_A 2
#define LH_ENCODER_B 4
//pins for motors
#define rightMotor_up 8
#define rightMotor_down 9
#define leftMotor_up 7
#define leftMotor_down 6
//pins for photo resistor, buttons & LED
#define light_Pin 7
#define led_Pin 10
#define buttonUp_Pin 11
#define buttonDown_Pin 12

// variables for motor encoder
volatile  long leftCount = 0;
volatile  long rightCount = 0;
unsigned long RH_prevTime;
unsigned long LH_prevTime;
unsigned long RH_time = 0;
unsigned long LH_time = 0;
int rightTop = 3000;
int leftTop = 3000;
int x = 0;
int stall = 10;

//variables for light sensing
int lowlight = 600;
int light;
long unsigned int sampletime = 1000;     //MAX VALUE = 4*10^9
long unsigned int lasttime = millis(); 

//variables for current state
int buttonOverride = 0;

void setup() {
  pinMode(LH_ENCODER_A, INPUT);
  pinMode(LH_ENCODER_B, INPUT);
  pinMode(RH_ENCODER_A, INPUT);
  pinMode(RH_ENCODER_B, INPUT);
  pinMode(rightMotor_up, OUTPUT);
  pinMode(rightMotor_down, OUTPUT);
  pinMode(leftMotor_up, OUTPUT);
  pinMode(leftMotor_down, OUTPUT);
  pinMode(buttonUp_Pin, INPUT);
  pinMode(buttonDown_Pin, INPUT);
  pinMode(led_Pin, OUTPUT);

  // initialize hardware interrupts
  attachInterrupt(digitalPinToInterrupt(LH_ENCODER_A), leftEncoderEvent, CHANGE);
  attachInterrupt(digitalPinToInterrupt(RH_ENCODER_A), rightEncoderEvent, CHANGE);

  digitalWrite(rightMotor_up, LOW);
  digitalWrite(rightMotor_down, LOW);
  digitalWrite(leftMotor_up, LOW);
  digitalWrite(leftMotor_down, LOW);

  Serial.begin(9600);

  
    light = analogRead(light_Pin);
    lasttime = millis();
    Serial.print("light: ");
    Serial.println(light);

}

void loop() {
  bool right = true;

Serial.println (millis()-lasttime);
if ((millis() - lasttime) > sampletime) {
    light = analogRead(light_Pin);
    lasttime = millis();
    Serial.print("light: ");
    Serial.println(light);
  }

  if (right){
    while (digitalRead(buttonUp_Pin)) {
      digitalWrite(rightMotor_up, HIGH);
      digitalWrite(rightMotor_down, LOW);
      
  Serial.print("count: ");
  Serial.print(rightCount);
  Serial.print("   time: ");
  Serial.println(RH_time);
    } 
    while (digitalRead(buttonDown_Pin)) {
      digitalWrite(rightMotor_down, HIGH);
      digitalWrite(rightMotor_up, LOW);
      
  Serial.print("   time: ");
  Serial.println(RH_time);
    }
  digitalWrite(rightMotor_up, LOW);
  digitalWrite(rightMotor_down, LOW);

  }else{
    while (digitalRead(buttonUp_Pin)) {
      digitalWrite(leftMotor_up, HIGH);
      digitalWrite(leftMotor_down, LOW);
      
  Serial.print("   time: ");
  Serial.println(LH_time);
    } 
    while (digitalRead(buttonDown_Pin)) {
      digitalWrite(leftMotor_down, HIGH);
      digitalWrite(leftMotor_up, LOW);
      
  Serial.print("   time: ");
  Serial.println(LH_time);
    }
  digitalWrite(leftMotor_up, LOW);
  digitalWrite(leftMotor_down, LOW);

  Serial.print("count: ");
  Serial.print(leftCount);
  Serial.print("   time: ");
  Serial.println(LH_time);
  }
  

}

void openBlinds()
{
  buttonOverride = 0;

  while (rightCount < rightTop || leftCount < leftTop) {
    if (rightCount < rightTop) {
      digitalWrite(rightMotor_up, HIGH);
    } else {
      digitalWrite(rightMotor_up, LOW);
    }
    if (leftCount < leftTop) {
      digitalWrite(leftMotor_up, HIGH);
    } else {
      digitalWrite(leftMotor_up, LOW);
    }
    if ((RH_time > stall && rightCount < rightTop) || (LH_time > stall && leftCount < leftTop)) {
      CheckStalled();
    }

    //check for button push
    if (digitalRead(buttonUp_Pin) || digitalRead(buttonDown_Pin)) {
      Serial.println("button stop");
      delay(200);
      buttonOverride = -1;
      break;
    }
  }
  digitalWrite(rightMotor_up, LOW);
  digitalWrite(leftMotor_up, LOW);
}


void closeBlinds()
{
  buttonOverride = 0;

  while (rightCount > 0 || leftCount > 0) {
    if (rightCount > 0) {
      digitalWrite(rightMotor_down, HIGH);
    } else {
      digitalWrite(rightMotor_down, LOW);
    }
    if (leftCount  > 0) {
      digitalWrite(leftMotor_down, HIGH);
    } else {
      digitalWrite(leftMotor_down, LOW);
    }

    if ((RH_time > stall && rightCount > 0) || (LH_time > stall && leftCount > 0)) {
      CheckStalled();
    }

    //check for button push
    if (digitalRead(buttonUp_Pin) == HIGH || digitalRead(buttonDown_Pin) == HIGH) {
      Serial.println("button stop");
      delay(200);
      buttonOverride = 1;
      break;
    }
  }
  digitalWrite(rightMotor_down, LOW);
  digitalWrite(leftMotor_down, LOW);
}

void ButtonPush(bool up, bool down) {
  delay(500);

  //if both buttons are held, ignore next day/night cycle
  if (digitalRead(buttonUp_Pin) && digitalRead(buttonDown_Pin)) {
    up = 0;
    down = 0;
    digitalWrite(led_Pin, HIGH);
    delay(3000);
    if (light < lowlight) {
      while (light < lowlight) {
        if (millis() % sampletime == 0) {
          light = analogRead(light_Pin);
        }
        if (digitalRead(buttonUp_Pin) ^ digitalRead(buttonDown_Pin)) {
          ButtonPush(digitalRead (buttonUp_Pin), digitalRead (buttonDown_Pin));
        }
      }
    } else {
      while (light > lowlight) {
        if (millis() % sampletime == 0) {
          light = analogRead(light_Pin);
        }
        if (digitalRead(buttonUp_Pin) ^ digitalRead(buttonDown_Pin)) {
          ButtonPush(digitalRead (buttonUp_Pin), digitalRead (buttonDown_Pin));
        }
      }
    }
    digitalWrite(led_Pin, LOW);
  }


  //if the button is held or was pushed
  while (digitalRead(buttonUp_Pin) || up) {
    openBlinds(); //button stop will delay 200ms, briefly stop motor (function as pwm) then return to this loop
    up = 0;
  }
  while (digitalRead(buttonDown_Pin)  || down) {
    closeBlinds();
    down = 0;
  }

  if (light < lowlight) {
    buttonOverride = 1;
  } else {
    buttonOverride = -1;
  }
}

void CheckStalled() {
  Serial.println("check stall");
  delay (15);
  if (RH_time > stall || LH_time > stall) {
    digitalWrite(led_Pin, HIGH);
    //stop all  motors
    digitalWrite(rightMotor_up, LOW);
    digitalWrite(rightMotor_down, LOW);
    digitalWrite(leftMotor_up, LOW);
    digitalWrite(leftMotor_down, LOW);

    //move blinds down
    digitalWrite(rightMotor_down, HIGH);
    digitalWrite(leftMotor_down, HIGH);
    delay (500);
    digitalWrite(rightMotor_down, LOW);
    digitalWrite(leftMotor_down, LOW);

    //move blinds back up
    digitalWrite(rightMotor_up, HIGH);
    digitalWrite(leftMotor_up, HIGH);
    delay (500);
    int  RH_last = RH_time;
    digitalWrite(rightMotor_up, LOW);
    int  LH_last = LH_time;
    digitalWrite(leftMotor_up, LOW);

    //check if right still stalled
    if (RH_last > stall && (rightTop - rightCount) < 500) {
      rightTop = rightCount;
    } else if (RH_time > stall) {
      //turn off all motors
      digitalWrite(rightMotor_up, LOW);
      digitalWrite(rightMotor_down, LOW);
      digitalWrite(leftMotor_up, LOW);
      digitalWrite(leftMotor_down, LOW);
      while (1) {
        Serial.println("Right motor stalled");
        digitalWrite(led_Pin, HIGH);
        delay (500);
        digitalWrite(led_Pin, LOW);
        delay (500);
      }
    }
    //check if left still stalled
    if (LH_last > stall && (leftTop - leftCount) < 500) {
      leftTop = leftCount;
    } else if (LH_time > stall) {
      //turn off all motors
      digitalWrite(rightMotor_up, LOW);
      digitalWrite(rightMotor_down, LOW);
      digitalWrite(leftMotor_up, LOW);
      digitalWrite(leftMotor_down, LOW);
      while (1) {
        Serial.println("Left motor stalled");
        digitalWrite(led_Pin, HIGH);
        delay (500);
        digitalWrite(led_Pin, LOW);
        delay (500);
      }
    }

    digitalWrite(led_Pin, LOW);
  }
}

// encoder event for the interrupt call
void leftEncoderEvent() {
  if (digitalRead(LH_ENCODER_A) == HIGH) {
    if (digitalRead(LH_ENCODER_B) == LOW) {
      leftCount++;
    } else {
      leftCount--;
    }
  } else {
    if (digitalRead(LH_ENCODER_B) == LOW) {
      leftCount--;
    } else {
      leftCount++;
    }
  }
  LH_time = millis() - LH_prevTime;
  LH_prevTime = millis();
}

// encoder event for the interrupt call
void rightEncoderEvent() {
  if (digitalRead(RH_ENCODER_A) == HIGH) {
    if (digitalRead(RH_ENCODER_B) == LOW) {
      rightCount++;
    } else {
      rightCount--;
    }
  } else {
    if (digitalRead(RH_ENCODER_B) == LOW) {
      rightCount--;
    } else {
      rightCount++;
    }
  }
  RH_time = millis() - RH_prevTime;
  RH_prevTime = millis();
}
