/*
Automatically opens/closes blinds with sun or at a buttonPin push. 
*/


#define RH_ENCODER_A 2 
#define RH_ENCODER_B 4
#define LH_ENCODER_A 3
#define LH_ENCODER_B 5
volatile unsigned long leftCount = 0;
volatile unsigned long rightCount = 0;

int rightMotor_up = 7;
int rightMotor_down = 6;
int lightPin = 0;
int buttonPin = 2;

int light;
int desiredState=-1; /*up=1, down=-1, middle=0*/
int rightTop=10;
int lowlight = 500;
int samplerate = 1000;
int movetime = 5000;  
unsigned long startmove;
unsigned long moved=0;
unsigned long starttime;

void setup()
{
  pinMode(rightMotor_up, OUTPUT);
  pinMode(rightMotor_down, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(LH_ENCODER_A, INPUT);
  pinMode(LH_ENCODER_B, INPUT);
  pinMode(RH_ENCODER_A, INPUT);
  pinMode(RH_ENCODER_B, INPUT);
  
  attachInterrupt(digitalPinToInterrupt(buttonPin), ButtonPush, RISING);
  digitalWrite(rightMotor_up, LOW);
  digitalWrite(rightMotor_down, LOW);

  
  while (!Serial);
  Serial.begin(9600);
  starttime=millis();
}

void loop()
{
      delay(samplerate);
      light=analogRead(lightPin);
      Serial.println(light); 
      
      if (light<lowlight && !closed){
        closeBlinds();
      }
      if (light>lowlight && closed){ 
        openBlinds();
      }  
      starttime=millis();
  }
}



void openBlinds()
{
    startmove=millis();
    digitalWrite(rightMotor_up, HIGH);
    while (moved<movetime){
      moved=millis()-startmove;
    }
    digitalWrite(rightMotor_up, LOW);
    moved=0;
    closed=0;  
}
void closeBlinds()
{
    startmove=millis();
    digitalWrite(rightMotor_down, HIGH);
    while (moved<movetime){
      moved=millis()-startmove;
    }
    digitalWrite(rightMotor_down, LOW);
    moved=0;
    closed=1;  
}

void ButtonPush()
{
  Serial.println("buttonPin pushed");
  if (closed){
    openBlinds();
  }
  if (!closed){
    closeBlinds();
  }
}
